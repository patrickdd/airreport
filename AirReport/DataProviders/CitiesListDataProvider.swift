//
//  CitiesListDataProvider.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 22.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

private let CityTableViewCellIdentifier = "CityTableViewCellIdentifier"
private let cityWasSelectedNotificationName = Notification.Name(" cityWasSelected")

class CitiesListDataProvider: NSObject, CitiesListProviderProtocol {
    
    private var cities = [City]()
    
    weak var tableView: UITableView! {
        
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
        }
    }
    
    init(withCities cities: [City] = City.getCities()) {
        
        self.cities = cities
    }
    
    // MARK: TableViewDelegate methods

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cityTableViewCell = tableView.dequeueReusableCell(withIdentifier: CityTableViewCellIdentifier, for: indexPath) as! CityTableViewCell
        cityTableViewCell.nameLabel.text = cities[indexPath.row].name
        
        return cityTableViewCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cities.count
    }
    
    // MARK: UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        NotificationCenter.default.post(name: cityWasSelectedNotificationName, object: nil, userInfo: ["selectedCity" : cities[indexPath.row]])
    }
    
    // MARK: public methods
    
    func reloadView() {
        
        tableView.reloadData()
    }
}
