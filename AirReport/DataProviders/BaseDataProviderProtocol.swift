//
//  BaseDataProviderProtocol.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 22.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

protocol BaseDataProviderProtocol: UITableViewDataSource, UITableViewDelegate {

    weak var tableView: UITableView! { get set }
}
