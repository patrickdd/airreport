//
//  CityAirDataProviderProtocol.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 21.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

protocol  CityAirDataProviderProtocol: BaseDataProviderProtocol {
    
    func reload(withCityAirData cityAirData: CityAirData)
}
