//
//  CityAirDataProvider.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 21.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

private let StationDataTableViewCellIdentifier = "StationDataTableViewCellIdentifier"

class CityAirDataProvider: NSObject,CityAirDataProviderProtocol {
    
    private var cityAirData: CityAirData?
    
    weak var tableView: UITableView! {
        
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
        }
    }
    
    // MARK: UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.backgroundColor = indexPath.row % 2 == 0  ? UIColor.solitude() : UIColor.lilyWhite()
    }
    
    // MARK: UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let stationDataTableViewCell = tableView.dequeueReusableCell(withIdentifier: StationDataTableViewCellIdentifier, for: indexPath) as! StationDataTableViewCell
        stationDataTableViewCell.setDataInViews(withStationData: cityAirData!.stationsData[indexPath.row])
        
        return stationDataTableViewCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let stationsData = cityAirData?.stationsData  else {
            
            return 0
        }
        
        return stationsData.count
    }
    
    // MARK: public methods
    
    func reload(withCityAirData cityAirData: CityAirData) {
        
        self.cityAirData = cityAirData
        tableView.reloadData()
    }
}
