//
//  CitiesListViewController.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 22.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

class CitiesListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    public var citiesListDataProvider: CitiesListProviderProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareDataProvider()
        tableView.setRoundedLightGreyBorder()
    }
    
    // MARK: private methods
    
    private func prepareDataProvider() {
        
        citiesListDataProvider = CitiesListDataProvider()
        citiesListDataProvider?.tableView = tableView
        citiesListDataProvider?.reloadView()
    }
    
    // MARK: action methods
    
    @IBAction func cancelButtonWasTapped(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
    
}
