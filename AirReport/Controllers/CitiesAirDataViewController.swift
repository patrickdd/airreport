//
//  ViewController.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 20.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

import UIKit

private let ShowCitiesListSegueIdentifier = "ShowCitiesListIdentifier"
private let cityWasSelectedNotificationName = Notification.Name(" cityWasSelected")

class CitiesAirDataViewController: UIViewController {

    @IBOutlet weak var selectCityButton: UIButton!
    public var cityAirDataProvider: CityAirDataProviderProtocol?
    @IBOutlet weak var tableView: UITableView!
    
    private var currentCity: City? {
        
        didSet {
            
            selectCityButton.setTitle(currentCity?.name ?? "Wybierz miasto", for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentCity = City.getCities().first
        
        prepareDataProvider()
        fetchCitiesAirData()
        adjustTableView()
        addAsObserver()
    }
    
    deinit {
        
         NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Action methods
    
    @IBAction func selectCityButtonWasTapped(_ sender: UIButton) {
        
        performSegue(withIdentifier: ShowCitiesListSegueIdentifier, sender: nil)
    }
    
    // MARK: internal methods
    
    func receiveCityWasSelectedNotification(withNotifications notification: NSNotification) {
        
        if let selectedCity = notification.userInfo?["selectedCity"] as? City {
            
            dismiss(animated: true, completion: nil)
            currentCity = selectedCity
            fetchCitiesAirData()
        }
    }
    
    // MARK: private methods
    
    private func adjustTableView() {
        
         tableView.rowHeight = UITableViewAutomaticDimension
         tableView.estimatedRowHeight = 112
         tableView.setRoundedLightGreyBorder()
    }
    
    private func prepareDataProvider() {
        
        cityAirDataProvider = CityAirDataProvider()
        cityAirDataProvider?.tableView = tableView
    }
    
    private func fetchCitiesAirData()  {
        
        SVProgressHUD.show()
        
        NetworkAssistant.shared.fetchCityAirData(cityId: currentCity?.ID , completionBlock: { [weak self] cityAirData, error in
            
            SVProgressHUD.dismiss()
            UIAlertController.show(withExtendedError: error)
            
            if let cityAirData = cityAirData {
                
                self?.cityAirDataProvider?.reload(withCityAirData: cityAirData)
            }
        })
    }
    
    private func addAsObserver() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(CitiesAirDataViewController.receiveCityWasSelectedNotification(withNotifications:)), name: cityWasSelectedNotificationName, object: nil)
    }
}

