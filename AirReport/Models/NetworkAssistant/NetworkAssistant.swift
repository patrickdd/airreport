//
//  NetworkAssistant.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 20.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

private let baseUrl = "http://powietrze.malopolska.pl/_powietrzeapi/api/dane"
typealias NetworkAsstatantGetCityAirData = (CityAirData?, ExtendedError?) -> ()

class NetworkAssistant: AFHTTPSessionManager {
    
    private static let sharedAssistant = NetworkAssistant(baseURL: URL(string:baseUrl ))
    
    static var shared : NetworkAssistant {
        
        return sharedAssistant
    }
    
    convenience init(url: URL?) {
        self.init(baseURL: url, sessionConfiguration: nil)
    
        responseSerializer.acceptableContentTypes?.insert("application/json")
    }
}

