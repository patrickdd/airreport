//
//  City.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 20.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

class City {
    
    private(set) var ID: Int
    private(set) var name: String
    
    init(ID: Int, name: String) {
        
        self.ID = ID
        self.name =  name
    }
    
    class func getCities() -> [City] {
        
        return [ City(ID: 1, name: "Kraków"),
                 City(ID: 2, name: "Tarnów"),
                 City(ID: 3, name: "Nowy Sącz"),
                 City(ID: 4, name: "Bochnia"),
                 City(ID: 5, name: "Brzesko"),
                 City(ID: 6, name: "Chrzanów"),
                 City(ID: 7, name: "Dąbrowa Tarnowska"),
                 City(ID: 8, name: "Gorlice"),
                 City(ID: 9, name: "Krynica Zdrój"),
                 City(ID: 10, name: "Limanowa"),
                 City(ID: 11, name: "Miechów"),
                 City(ID: 12, name: "Myślenice"),
                 City(ID: 13, name: "Nowy Targ"),
                 City(ID: 14, name: "Olkusz"),
                 City(ID: 15, name: "Oświęcim"),
                 City(ID: 16, name: "Proszowice"),
                 City(ID: 17, name: "Skawina"),
                 City(ID: 18, name: "Sucha Beskidzka"),
                 City(ID: 19, name: "Trzebinia"),
                 City(ID: 20, name: "Tuchów"),
                 City(ID: 21, name: "Wadowice"),
                 City(ID: 22, name: "Wieliczka"),
                 City(ID: 23, name: "Zakopane"),
                 City(ID: 24, name: "Gmina Kłaj")]
    }
}
