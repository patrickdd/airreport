//
//  Error.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 21.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

class ExtendedError: Swift.Error {
    
    var errorDescription: String = ""
    
    enum ExtendedType: Swift.Error {
        
        case dataNotFound
        
        var message: String {
            
            switch self {
            case .dataNotFound:
                return "Nie znaleziono danych"
                
            }
        }
    }
    
    init(withExtendedType type: ExtendedType) {
        
        errorDescription = type.message
    }
    
    init(withError error: Error) {
        
        errorDescription = error.localizedDescription
    }
}
