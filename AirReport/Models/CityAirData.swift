//
//  CityAirData.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 21.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

enum AirQuality: Int {
    
    case noData = 0
    case veryGood
    case good
    case moderate
    case sufficient
    case bad
    case veryBad
    
    var pictogram: UIImage {
        
        var currentPictogram = UIImage()
        
        switch self {
        case .noData:
            currentPictogram = UIImage(named: "noDataPictogram")!
        case .veryGood:
            currentPictogram = UIImage(named: "veryGoodPictogram")!
        case .good:
            currentPictogram = UIImage(named: "goodPictogram")!
        case .moderate:
            currentPictogram = UIImage(named: "moderatePictogram")!
        case .sufficient:
            currentPictogram = UIImage(named: "sufficientPictogram")!
        case .bad:
            currentPictogram = UIImage(named: "badPictogram")!
        case .veryBad:
            currentPictogram = UIImage(named: "veryBadPictogram")!
        }
        
        return currentPictogram
    }
}

class StationDataDetails {
    
    private (set) var pollutionType = ""
    private (set) var pollutionValue = 0
    private (set) var measurementTime = ""
    private (set) var measurementHour = ""
    private (set) var giosIndex = ""
    private (set) var caqiIndex = ""
    private (set) var aqiIndex = ""
    private (set) var parametrDescription = ""
    
    init(withDictionary dictionary: [AnyHashable : Any]) {
        
        if let pollutionType = dictionary["o_wskaznik"] as? String{
            
            self.pollutionType = pollutionType
        }
        
        if let pollutionValue = dictionary["o_value"] as? Int{
            
            self.pollutionValue = pollutionValue
        }
        
        if let measurementTime = dictionary["o_czas"] as? String{
            
            self.measurementTime = measurementTime
        }
        
        if let measurementHour = dictionary["o_time"] as? String{
            
            self.measurementHour = measurementHour
        }
        
        if let giosIndex = dictionary["g_index"] as? String{
            
            self.giosIndex = giosIndex
        }
        
        if let caqiIndex = dictionary["caqi_id"] as? String{
            
            self.caqiIndex = caqiIndex
        }
        
        if let aqiIndex = dictionary["aqi_id"] as? String{
            
            self.aqiIndex = aqiIndex
        }
        
        if let descriptionOfParametr = dictionary["par_desc"] as? String {
            
            parametrDescription = descriptionOfParametr
        }
    }
}

class StationData {
    
    private (set) var stationId = 0
    private (set) var stationName = ""
    private (set) var stationHour = 0
    private (set) var stationMax: AirQuality?
    
    var stationDataDetailsArray = [StationDataDetails]()
    
    init(withDictionary dictionary: [AnyHashable : Any]) {
        
        if let stationIdString = dictionary["station_id"] as? String, let stationId = Int(stationIdString) {
            
            self.stationId = stationId
        }
        
        if let stationName = dictionary["station_name"] as? String{
            
            self.stationName = stationName
        }
        
        if let stationHour = dictionary["station_hour"] as? Int {
            
            self.stationHour = stationHour
        }
        
        if let max = dictionary["station_max"] as? String, let maxValue = Int(max) {
            
            self.stationMax = AirQuality(rawValue: maxValue)!
        }
        
        if let detailsDictionaries = dictionary["details"] as? [[AnyHashable : Any]] {
            
            for dictionary in detailsDictionaries {
                
                stationDataDetailsArray.append(StationDataDetails(withDictionary: dictionary))
            }
        }
    }
}

class CityAirData {
    
    var stationsData = [StationData]()
    
    init(withDictionary dictionary: [AnyHashable : Any]) {
        
        if let stationsArray = (dictionary["dane"] as? [AnyHashable : Any])?["actual"] as? [[AnyHashable : Any]]  {
            
            for stationDictionary in stationsArray {
                
                stationsData.append(StationData(withDictionary: stationDictionary))
            }
        }
    }
}


