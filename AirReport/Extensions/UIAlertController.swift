//
//  UIAlertController.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 21.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

extension UIAlertController {
    
    class func show(withExtendedError error: ExtendedError?){
        
        guard let error = error else {
            
            return
        }
        
        let alertController = UIAlertController(title: "", message: error.errorDescription, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        UIViewController.topViewController()?.present(alertController, animated: true, completion: nil)
    }
}
