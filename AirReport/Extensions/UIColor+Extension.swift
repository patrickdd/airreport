//
//  UIColor+Extension.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 22.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

extension UIColor {
    
    class func solitude() -> UIColor {
        
        return UIColor(red: 225.0/255.0, green: 228.0/255.0, blue: 232.0/255.0, alpha: 1.0)
    }
    
    class func lilyWhite() -> UIColor {
        
        return UIColor(red: 247.0/255.0, green: 249.0/255.0, blue: 248.0/255.0, alpha: 1.0)
    }
    
    class func lightGrey() -> UIColor {
    
        return UIColor(red: 215.0/255.0, green: 212.0/255.0, blue: 214.0/255.0, alpha: 1.0)
    }
}
