//
//  NetworkAssistant+CityAirData.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 20.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

extension NetworkAssistant {
    
    func fetchCityAirData(cityId: Int? = 1, completionBlock: @escaping NetworkAsstatantGetCityAirData) {
        
        _ = get("", parameters: prepareParametersDictionary(cityId: cityId!), progress: nil, success: { sessionDataTask, response in
            
            if let dataDictionary = response as? [AnyHashable : Any]{
                
                completionBlock(CityAirData(withDictionary: dataDictionary), nil)
            }
            else {
                completionBlock(nil, ExtendedError(withExtendedType: .dataNotFound))
            }
        }) { sessionDataTask, error in
            
            completionBlock(nil, ExtendedError(withError: error))
        }
    }
    
    // MARK: private methods
    
    private func prepareParametersDictionary(cityId: Int) -> [AnyHashable : Any] {
        
        return [
            "act" : "danemiasta",
            "ci_id" : cityId
            ] as [AnyHashable : Any]
    }
}
