//
//  UIViewController+Extension.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 21.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

extension UIViewController {
    
    class func topViewController(withController controller: UIViewController? =  UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let navigationController = controller as? UINavigationController {
            
            return topViewController(withController: navigationController.visibleViewController)
        } else if let presentedViewController = controller?.presentedViewController {
            
            return topViewController(withController: presentedViewController)
        }else if let tabBarController = controller as? UITabBarController {
            
            return topViewController(withController: tabBarController.selectedViewController)
        }
        
        return controller
    }
}
