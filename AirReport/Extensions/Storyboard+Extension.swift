//
//  Storyboard+Extension.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 21.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

extension UIStoryboard {
    
    class func instantatieViewController( inStoryboard storyboardName: String, withIdentifier controllerIdentifier: String) -> UIViewController  {
        
        return UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: controllerIdentifier)
    }
}

