//
//  StationDataTableViewCell.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 22.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

class StationDataTableViewCell: UITableViewCell {
    
    @IBOutlet weak var stationNameLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var pictogramImageView: UIImageView!
    
    public func setDataInViews(withStationData stationData: StationData) {
        
        stationNameLabel.text = stationData.stationName
    
        hourLabel.text = "Godzina: \(stationData.stationHour)"
    
        pictogramImageView.image = stationData.stationMax?.pictogram
        
        detailsLabel.text = stationData.stationDataDetailsArray.map( { "\($0.parametrDescription) \($0.pollutionType): \($0.pollutionValue) " }).joined(separator: " \n")
    }
}
