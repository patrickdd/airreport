//
//  CityTests.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 20.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

@testable import AirReport
import XCTest

class CityTests: XCTestCase {
    
    func testProperInitialisation() {
        
        let city = City(ID: 1, name: "Kraków")
        
        XCTAssertEqual(city.ID, 1, "ID should be same like in creation")
        XCTAssertEqual(city.name, "Kraków", "Name should be same like in creation")
    }
    
    func testCitiesListShouldContainTwentyFourCities(){
        
        XCTAssertTrue(City.getCities().count == 24)
    }
}
