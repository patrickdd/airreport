//
//  NetworkAssistantTests.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 20.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

@testable import AirReport
import XCTest

class NetworkAssistantTests: XCTestCase {
    
    func testSharedIsNotNil() {
  
        XCTAssertNotNil(NetworkAssistant.shared)
    }
    
    func testCheckAcceptableContentTypes() {
        
        guard let acceptableContentTypes = NetworkAssistant.shared.responseSerializer.acceptableContentTypes else {
            
            XCTFail()
            return
        }
        
        XCTAssertTrue(acceptableContentTypes.contains("application/json"))
    }
}
