//
//  CitiesAirDataViewControllerTests.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 21.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

@testable import AirReport
import XCTest

private let CitiesAirDataViewControllerIdentifier = "CitiesAirDataViewControllerIdentifier"

class CitiesAirDataViewControllerTests: XCTestCase {
    
    var citiesAirDataViewController: CitiesAirDataViewController!
    
    override func setUp() {
        super.setUp()
       
       citiesAirDataViewController = UIStoryboard.instantatieViewController(inStoryboard: "Main", withIdentifier: CitiesAirDataViewControllerIdentifier) as! CitiesAirDataViewController
        _ = citiesAirDataViewController.view
    }
    
    override func tearDown() {
      
        super.tearDown()
    }
    
    func testHaveSelectCityButton() {
        
        XCTAssertNotNil(citiesAirDataViewController.selectCityButton)
    }
    
    func testSelectButtonIsConnectedWithRightMethod() {
        
        guard let actions = citiesAirDataViewController.selectCityButton.actions(forTarget: citiesAirDataViewController, forControlEvent: .touchUpInside) else {
            
            XCTFail()
            return
        }
        
        XCTAssertTrue(actions.contains("selectCityButtonWasTapped:"))
    }
    
    func testTitleOfSelectCityAfterViewDidLoad() {
        
        let selectCityButton = citiesAirDataViewController.selectCityButton
        
        XCTAssertEqual(selectCityButton?.title(for: .normal), "Kraków", "Title of button should be equal to name of first element")
    }
    
    func testControllerIsNotNil() {
        
        XCTAssertNotNil(citiesAirDataViewController)
    }
    
    func testCitiesAirDataViewControllerShouldHaveTableView() {
        
        XCTAssertNotNil(citiesAirDataViewController.tableView, "citiesAirDataViewController should have tableView")
    }
    
    func testCitiesAirDataViewControllerShouldHaveDataProvider() {
        
         XCTAssertNotNil(citiesAirDataViewController.cityAirDataProvider, "CitiesAirDataViewController should have data provider")
    }
    
    func testAfterViewLoadControllersDataContainerShouldHaveTableView() {
        
        guard let cityAirDataProvider = citiesAirDataViewController.cityAirDataProvider else {
            
            XCTFail()
            return
        }
        
        XCTAssertNotNil(cityAirDataProvider.tableView, "Data provider's tableView should not be nil")
    }
    
    func testDataProviderShouldBeDataSourceForTableView() {
        
        guard let tableView = citiesAirDataViewController.tableView else {
            
            XCTFail()
            return
        }
        
        XCTAssertNotNil(tableView.dataSource)
        XCTAssertNotNil(tableView.dataSource is CityAirDataProvider, "City air data provider should be source of table view")
    }
}
