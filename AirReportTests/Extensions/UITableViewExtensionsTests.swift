//
//  UITableViewExtensionsTests.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 23.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

@testable import AirReport
import XCTest

class UITableViewExtensionsTests: XCTestCase {
       
    func testCheckProperlyConfigurationAfterSetRoundedGreyBorder() {
        
        let tableView = UITableView()
        
        tableView.setRoundedLightGreyBorder()
        
        XCTAssertTrue(tableView.layer.borderWidth == 2)
        XCTAssertTrue(tableView.layer.borderColor == UIColor.lightGray.cgColor)
        XCTAssertTrue(tableView.layer.cornerRadius == 6.0)
    }
}
