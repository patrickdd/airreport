//
//  CityAirDataProviderTests.swift
//  AirReport
//
//  Created by Patryk.mac.minii on 22.03.2017.
//  Copyright © 2017 Patryk Domalik. All rights reserved.
//

@testable import AirReport
import XCTest

private let CitiesAirDataViewControllerIdentifier = "CitiesAirDataViewControllerIdentifier"

class CityAirDataProviderTests: XCTestCase {
    
    var cityAirDataProvider: CityAirDataProviderProtocol!
     var citiesAirDataViewController: CitiesAirDataViewController!
    
    override func setUp() {
        super.setUp()

        citiesAirDataViewController = UIStoryboard.instantatieViewController(inStoryboard: "Main", withIdentifier: CitiesAirDataViewControllerIdentifier) as! CitiesAirDataViewController
        _ = citiesAirDataViewController.view
        
        cityAirDataProvider = CityAirDataProvider()
        citiesAirDataViewController.cityAirDataProvider = cityAirDataProvider
        cityAirDataProvider.tableView = citiesAirDataViewController.tableView
    }
    
    func testNumberOfRowsBeforeAddReloadWithData() {
        
        let tableView = cityAirDataProvider.tableView
        
        XCTAssertTrue(tableView?.numberOfRows(inSection: 0) == 0, "Number of of rows before add Users should be 0")
    }
    
    func testDataProviderShouldBeDataDelegateForTableView() {
        
        guard let tableView = citiesAirDataViewController.tableView else {
            
            XCTFail()
            return
        }
        
        XCTAssertNotNil(tableView.delegate)
        XCTAssertNotNil(tableView.delegate is CityAirDataProvider, "City air data provider should be delegate of table view")
    }
    
    func testRightNumberOfCellsAfterAddData() {
        
        let tableView = cityAirDataProvider.tableView
        
        XCTAssertTrue(tableView?.numberOfRows(inSection: 0) == 0)
        
        cityAirDataProvider.reload(withCityAirData: CityAirData(withDictionary: getCityAirDataDictionary()))
        
        XCTAssertTrue(tableView?.numberOfRows(inSection: 0) == 1)
    }
    
    func testTypeOfCellInTableView() {
        
        let tableView = cityAirDataProvider.tableView
        
        cityAirDataProvider.reload(withCityAirData: CityAirData(withDictionary: getCityAirDataDictionary()))
        
        let cell = tableView?.cellForRow(at: IndexPath(row: 0, section: 0))
        
        XCTAssertTrue(cell is StationDataTableViewCell, "Cell should be StationDataTableViewCell type")
    }
    
    func testDataInCell() {
        
        let tableView = cityAirDataProvider.tableView
        
        cityAirDataProvider.reload(withCityAirData: CityAirData(withDictionary: getCityAirDataDictionary()))
        
        guard let cell = tableView?.cellForRow(at: IndexPath(row: 0, section: 0)) as? StationDataTableViewCell else {
            
            XCTFail()
            return
        }
        
        XCTAssertEqual(cell.stationNameLabel.text , "Kraków")
        XCTAssertEqual(cell.hourLabel.text , "Godzina: 12")
    }
    
    // MARK: private methods
    
    private func getCityAirDataDictionary() -> [AnyHashable : Any] {
        
        return [ "dane": [ "actual" : [ ["station_id" : 1, "station_name" : "Kraków", "station_hour" : 12]]]]
    }
}
